package grid

func (g *Grid) isRowValid(row int) (bool) {
	return row >= 0 && row < g.RowCount
}

func (g *Grid) isColValid(col int) (bool) {
	return col >= 0 && col < g.ColCount
}

func (g *Grid) isValid(row int, col int) (bool) {
	return g.isRowValid(row) && g.isColValid(col)
}
