package main

import (
	"gitlab.com/kurczynski/game-of-life/pkg/grid"
	"gitlab.com/kurczynski/game-of-life/pkg/process"
	"time"
)

func main() {
	rows := 10
	cols := 10

	g := grid.New(rows, cols)
	g.Seed()

	for true {
		grid.Print(g)

		g = process.Process(g)

		time.Sleep(time.Millisecond * 500)
	}
}
