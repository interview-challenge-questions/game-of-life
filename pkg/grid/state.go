package grid

func isUnderPop(nAlive int) (bool) {
	return nAlive < 2
}

func isNextGen(nAlive int) (bool) {
	return nAlive == 2 || nAlive == 3
}

func isOverPop(nAlive int) (bool) {
	return nAlive > 3
}

func isReproduction(nAlive int) (bool) {
	return nAlive == 3
}

// Return the state of the given cell with the population constraints
func GetState(cState State, nLiving int) (state State) {
	if cState.State {
		if isUnderPop(nLiving) || isOverPop(nLiving) {
			state.State = false
		} else if isNextGen(nLiving) {
			state.State = true
		}
	} else {
		if isReproduction(nLiving) {
			state.State = true
		} else {
			state.State = false
		}
	}

	return state
}
