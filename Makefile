MAIN := 'cmd/life/main.go'
BIN := 'life'

build:
	go build -o $(BIN) $(MAIN)

clean:
	go clean ./...
