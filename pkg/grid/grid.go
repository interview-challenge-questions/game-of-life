package grid

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

type Grid struct {
	ColCount int
	RowCount int
	grid     [][]bool
}

type Location struct {
	Row int
	Col int
}

type State struct {
	State bool
}

func New(rowCount int, colCount int) (*Grid) {
	grid := make([][]bool, rowCount)

	for row, _ := range grid {
		grid[row] = make([]bool, colCount)
	}

	return &Grid{
		ColCount: len(grid[0]),
		RowCount: len(grid),
		grid:     grid,
	}
}

// Seed a grid with random cells that are alive
func (g *Grid) Seed() () {
	for row, _ := range g.grid {
		for col, _ := range g.grid[row] {
			seed := rand.NewSource(time.Now().UnixNano())
			num := rand.New(seed)

			var state bool

			if num.Int()%2 == 0 {
				state = true
			} else {
				state = false
			}

			g.grid[row][col] = state
		}
	}
}

// Get the location of the neighbors for the given location
func (g *Grid) GetNeighbors(l Location) ([]Location) {
	return []Location{
		// Top
		{Row: l.Row - 1, Col: l.Col},
		// Top-right
		{Row: l.Row - 1, Col: l.Col + 1},
		// Right
		{Row: l.Row, Col: l.Col + 1},
		// Bottom-right
		{Row: l.Row + 1, Col: l.Col + 1},
		// Bottom
		{Row: l.Row + 1, Col: l.Col},
		// Bottom-left
		{Row: l.Row + 1, Col: l.Col - 1},
		// Left
		{Row: l.Row, Col: l.Col - 1},
		// Top-left
		{Row: l.Row - 1, Col: l.Col - 1},
	}
}

// Return the states of all the given neighbor locations
func (g *Grid) GetNeighborStates(location ...Location) (states []State) {
	for _, i := range location {
		if g.isValid(i.Row, i.Col) {
			states = append(states, g.Get(i))
		} else {
			continue
		}
	}

	return
}

func (g *Grid) Set(location Location, state State) {
	g.grid[location.Row][location.Col] = state.State
}

func (g *Grid) Get(location Location) (state State) {
	state.State = g.grid[location.Row][location.Col]

	return
}

// Clear the current terminal and print the state of the given grid
func Print(g *Grid) {
	// Note that this will only work when using an ANSI terminal, more details here:
	// https://en.wikipedia.org/wiki/ANSI_escape_code#Sequence_elements
	// Move cursor to position 0, 0 (top left)
	fmt.Printf("\033[0;0H")
	// Clear screen
	fmt.Printf("\033[2J")

	cellWidth := 3
	colBorder := strings.Repeat(`-`, (g.ColCount*cellWidth)+2)

	fmt.Printf("%s\n", colBorder)

	for row, _ := range g.grid {
		fmt.Printf(`|`)

		for col, _ := range g.grid {
			if g.grid[row][col] {
				fmt.Printf(" X ")
			} else {
				fmt.Printf("   ")
			}
		}

		fmt.Printf("|\n")
	}

	fmt.Printf("%s\n", colBorder)
}

