# game-of-life
Go implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).

## Build
```
make
```

## Run
```
./life
```
Note that this application must be run from an ANSI terminal because of the way
it handles clearing the screen.
