package process

import "gitlab.com/kurczynski/game-of-life/pkg/grid"

// Iterate through the grid and apply the rules to each cell based on their neighbor state
func Process(g *grid.Grid) (newGrid *grid.Grid) {
	var n []grid.Location
	var nStates []grid.State
	var state grid.State
	var current grid.Location

	newGrid = grid.New(g.RowCount, g.ColCount)

	for i := 0; i < g.RowCount; i++ {
		for j := 0; j < g.ColCount; j++ {
			current = grid.Location{i, j}

			n = g.GetNeighbors(current)
			nStates = g.GetNeighborStates(n...)

			living := countLiving(nStates...)
			state = grid.GetState(g.Get(current), living)

			newGrid.Set(current, state)
		}
	}

	return
}

func countLiving(neighbors ...grid.State) (nLiving int) {
	for _, i := range neighbors {
		if i.State {
			nLiving++
		}
	}

	return
}
